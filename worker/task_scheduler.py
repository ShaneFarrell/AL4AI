from threading import Thread, get_ident, main_thread
from queue import Queue
import requests
from worker.settings import WEBSITE_ADDRESS, THREAD_COUNT
import traceback
from django.utils.timezone import localtime

thread_storage = {
    "status": {},
    "project_id": {},
    "task_type": {},
    "task_start": {},
}

main_thread_results = []
main_thread_stats = []


class TaskScheduler:
    def __init__(self, thread_count):
        self.task_queue = Queue()
        self.threads: list[Thread] = [
            Thread(target=TaskScheduler._thread, args=(self.task_queue,), daemon=True)
            for _ in range(thread_count)
        ]
        for thread in self.threads:
            thread.start()

    def schedule_task(self, project_id, task_type, task_function, /, **kwargs):
        self.task_queue.put((project_id, task_type, task_function, kwargs))

    @staticmethod
    def _thread(task_queue: Queue):
        thread_id = get_ident()
        thread_storage["status"][thread_id] = "idle"

        while True:
            project_id, task_type, target, kwargs = task_queue.get()
            thread_storage["task_start"][thread_id] = localtime()
            thread_storage["status"][thread_id] = f"running ({task_type})"
            thread_storage["task_type"][thread_id] = task_type
            thread_storage["project_id"][thread_id] = project_id

            try:
                results = target(**kwargs)
            except Exception as e:
                requests.post(WEBSITE_ADDRESS + "/worker/error", json={
                    "type": task_type, "data": str(e), "id": project_id
                })
                traceback.print_exc()
                thread_storage["status"][thread_id] = "idle (error)"
            else:
                if results is not None:
                    send_results(results)
                thread_storage["status"][thread_id] = "idle"


def send_stats(stats: dict, *, thread_id=None):
    print("Stats: [" + "] [".join(f"{key}: {value}" for key, value in stats.items()) + "]")

    if thread_id is None:
        thread_id = get_ident()

    if thread_id != main_thread().ident:
        task_type = thread_storage["task_type"][thread_id]
        project_id = thread_storage["project_id"][thread_id]

        requests.post(WEBSITE_ADDRESS + "/worker/stats", json={
            "type": task_type,
            "data": stats,
            "id": project_id,
            "task_time": str(thread_storage["task_start"][thread_id])
        })
    else:
        main_thread_stats.append(stats)


def send_results(results):
    global main_thread_results
    print("Results:", results)

    thread_id = get_ident()
    if thread_id != main_thread().ident:
        task_type = thread_storage["task_type"][thread_id]
        project_id = thread_storage["project_id"][thread_id]

        requests.post(WEBSITE_ADDRESS + "/worker/results", json={
            "type": task_type, "data": results, "id": project_id
        })
        thread_storage["status"][thread_id] = f"post processing ({task_type})"
    else:
        main_thread_results.append(results)


task_scheduler = TaskScheduler(THREAD_COUNT)
