import numpy as np
from modAL.models.learners import ActiveLearner

from random import choices

from time import time
from worker.dataset import get_dataset
from worker.models.query_strategy import get_query_strategy
from worker.models.machine_learning_model import get_machine_learning_model
from worker.models.representation import get_representation
from worker.task_scheduler import send_stats, send_results
from worker.models.evaluation import evaluate_model


def run_active_learning(settings, label_map, *, force_random_results=False):
    dataset = get_dataset(settings["DatasetLocation"])
    print("label map:", label_map)

    unlabelled_urls = dataset.get_unlabelled_urls(label_map)
    labelled_urls = dataset.get_labelled_urls(label_map)
    labels = dataset.get_labels(label_map)

    unique_labels = list(set(labels))

    print("Labels", len(unlabelled_urls), len(labelled_urls), len(labels))
    labelled_count = len(labelled_urls)
    dataset_size = len(unlabelled_urls) + labelled_count

    send_stats({
        "last_run": time(),
        "dataset_size": dataset_size,
        "labelled_count": labelled_count,
        "labelled_percentage": int((labelled_count / max(1, dataset_size)) * 10_000) / 100,
    })

    if not unlabelled_urls:
        send_results([])
        return

    print("settings:", settings)
    max_task_count = int(settings.get("BatchSize", 1))

    query_strategy = get_query_strategy(settings.get("QueryStrategy"))

    if (not labelled_urls) or (len(unique_labels) < 2) or (query_strategy is None):
        send_results(choices(unlabelled_urls, k=max_task_count))
        return

    representation = get_representation(settings.get("Representation"), dataset, settings)
    training_inputs = representation.train(labelled_urls)

    print(labels)
    training_targets = np.array([unique_labels.index(label) for label in labels])

    send_stats({
        "progress": "Yes",
    })

    print(training_inputs.shape, unique_labels)

    model = get_machine_learning_model(settings.get("MachineLearningModel"), len(labels))

    print(model, query_strategy)
    learner = ActiveLearner(
        estimator=model,
        query_strategy=query_strategy,
        X_training=training_inputs, y_training=training_targets
    )

    print(learner.score(training_inputs, training_targets))

    evaluation_inputs = representation.test(unlabelled_urls)
    print("almost done")
    query_idx, query_inst = learner.query(
        evaluation_inputs,
        n_instances=min(max_task_count, len(unlabelled_urls))
    )

    if force_random_results:
        send_results(choices(unlabelled_urls, k=max_task_count))
    else:
        send_results([unlabelled_urls[idx] for idx in query_idx])

    send_stats({
        "accuracy": evaluate_model(settings, model, representation, unique_labels)
    })
