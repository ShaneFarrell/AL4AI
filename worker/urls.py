from django.urls import path
from worker.views import run_view, home_view, dataset_view


urlpatterns = [
    path("", home_view),
    path("run/", run_view),
    path("dataset/", dataset_view),
]
