import requests
from worker.settings import CACHE_SIZE
from threading import Thread, get_ident
from queue import Queue, Empty
from time import time, sleep
from worker.task_scheduler import send_stats
from django.utils.timezone import localtime

_cache = {}
current_cache_size = 0
_last_report_time = 0
_cache_clear_thread = None


def _clear_cache_daily():
    global _cache_clear_thread

    def thread():
        global current_cache_size
        while True:
            sleep(60 * 30)  # Sleep for 30 minutes

            # Clear the cache at 4am
            if localtime().hour == 4:
                current_cache_size = 0
                _cache.clear()
                sleep(60 * 60 * 20)  # Sleep for 20 hours

    if _cache_clear_thread is None:
        _cache_clear_thread = Thread(target=thread, daemon=True)
        _cache_clear_thread.start()


def cached_get(url):
    global current_cache_size
    _clear_cache_daily()
    content = _cache.get(url)

    if content is None:
        content = _cache[url] = requests.get(url).content
        current_cache_size += len(content)

    if current_cache_size > CACHE_SIZE:
        current_cache_size = 0
        _cache.clear()

    return content


def cached_get_multiple(urls):
    results = []
    url_queue = Queue()

    for url in urls:
        try:
            results.append(_cache[url])
        except KeyError:
            url_queue.put(url)

    threads = []
    thread_id = get_ident()

    for _ in range(min(len(urls) - len(results), 100)):
        threads.append(Thread(target=_thread, args=(url_queue, results, len(urls), thread_id)))
        threads[-1].start()

    for thread in threads:
        thread.join()

    send_stats({"progress": f"Loading url {len(results)}/{len(urls)}"})

    return results


def _thread(url_queue: Queue, results: list, url_count, thread_id):
    global _last_report_time
    try:
        while True:
            results.append(cached_get(url_queue.get_nowait()))
            if time() > _last_report_time + 5:
                _last_report_time = time()
                send_stats({"progress": f"Loading url {len(results)}/{url_count}"}, thread_id=thread_id)
    except Empty:
        pass
