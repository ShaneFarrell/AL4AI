from worker.dataset import get_dataset


def evaluate_model(settings, model, representation, unique_labels):
    if not settings.get("TestDatasetLocation"):
        return None

    test_dataset = get_dataset(settings["TestDatasetLocation"])
    test_urls = test_dataset.get_all_urls()

    test_inputs = test_dataset.load_urls(test_urls, representation, False)

    if not test_inputs.shape:
        return None

    test_predictions = [unique_labels[index] for index in model.predict(test_inputs)]
    ground_truth = [test_dataset.get_target(url) for url in test_urls]

    correct_answers = [
        prediction.lower() == correct_answer.lower()
        for correct_answer, prediction in zip(ground_truth, test_predictions)
    ]

    print([test_dataset.get_target(url) for url in test_urls])
    print(test_predictions)

    accuracy = sum(correct_answers) / len(correct_answers)
    print(f"{settings['MachineLearningModel']} accuracy:", accuracy)

    return accuracy
