function autoSubmit(event) {
    document.querySelector("#auto-submit").click()
}

document.addEventListener("DOMContentLoaded", function(event) {
    document.querySelectorAll(".auto-refresh").forEach(node =>
        node.addEventListener("change", autoSubmit)
    );
});
