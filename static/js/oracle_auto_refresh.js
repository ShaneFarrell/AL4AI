function autoRefresh(event) {
    fetch(window.location + "/ping")
        .then((response) => response.text())
        .then((response) => {
            const timer = Number(response);
            console.log(timer);
            if (timer === 0) {
                window.location.reload();
            } else if (timer > 0) {
                window.setTimeout(autoRefresh, 1000 * timer);
            }
        });
}

document.addEventListener("DOMContentLoaded", function(event) {
    window.setTimeout(autoRefresh, 1000);
});
