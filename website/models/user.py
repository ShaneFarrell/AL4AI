from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
import uuid


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)

        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        if extra_fields.setdefault('is_staff', True) is not True:
            raise ValueError('Superuser must have is_staff=True.')

        if extra_fields.setdefault('is_superuser', True) is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """User model."""

    username = None
    email = models.EmailField("email address", unique=True)
    worker_url = models.URLField("worker url", null=True, max_length=2000)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def create_project(self, name: str):
        from . import Project
        from website.interface.component_types import all_components

        if not name:
            raise ValueError("Empty project name")

        name = name[:Project.name.field.max_length]

        if self.get_project(name):
            raise KeyError(f"Project name '{name}' in use")

        project = Project(user=self, name=name)

        for component in all_components:
            component = component(project)
            component.submit(component.value)

        project.save()

        return project

    def get_project(self, name):
        if not name:
            return None

        return self.project_set.filter(name=name).first()

    @property
    def most_recent_project(self):
        return self.project_set.order_by("-updated").first()

    @property
    def project_names(self):
        return [project.name for project in self.project_set.all()]
