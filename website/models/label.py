from django.db import models
from . import Project
import uuid


class Label(models.Model):
    csv_optional_fields = ["batch", "oracle", "created"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    created = models.DateTimeField("created", auto_now_add=True)
    updated = models.DateTimeField("created", auto_now=True)

    oracle_name = models.CharField(max_length=512)

    url = models.URLField("datum url", max_length=2000)
    value = models.TextField("value")

    def __str__(self):
        return f"{self.project} --- {self.url}"
