from django.db import models
from . import User
from django.utils.timezone import localtime
import uuid
from base64 import urlsafe_b64encode
from hashlib import sha256


class Project(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    updated = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=35)

    settings = models.JSONField(default=dict)
    stats = models.JSONField(default=list)

    run_until = models.DateTimeField(auto_now_add=True)

    predicted_labels = models.JSONField(default=None, null=True)
    test_labels = models.JSONField(default=None, null=True)
    download_count = models.PositiveSmallIntegerField(default=0)

    def get_labels(self):
        return {
            label.url: label.value
            for label in self.label_set.all()
        }

    @property
    def running(self):
        return self.run_until > localtime()

    @property
    def state(self) -> str:
        if self.running:
            return "Running"

        return "Idle"

    def add_label(self, url, value, oracle_name):
        from website.models import Label
        label = Label(url=url, value=value, oracle_name=oracle_name, project=self)
        label.save()

        return label

    def set_oracles(self, names: list[str]):
        from . import Oracle

        existing_names = set()

        for oracle in self.oracle_set.all():
            if oracle.name not in names:
                oracle.delete()
            else:
                existing_names.add(oracle.name)

        for name in set(names) - existing_names:
            oracle_id = str(urlsafe_b64encode(sha256(self.id.bytes + bytes(name, "utf-8")).digest()), "utf-8")
            oracle = Oracle(id=oracle_id, name=name, project=self)
            oracle.save()

    @property
    def latest_stats(self):
        stats = {}
        for entry in self.stats:
            stats.update(entry)
        return stats

    def __str__(self):
        return f"{self.user} --- {self.name}"
