from django.db import models
from . import Project
from uuid import uuid4
from base64 import urlsafe_b64encode
from hashlib import sha256


def url_safe_uuid():
    return urlsafe_b64encode(sha256(uuid4().bytes).digest())


class Oracle(models.Model):
    id = models.CharField(primary_key=True, default=url_safe_uuid, max_length=44, editable=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    name = models.CharField(max_length=512)
    label_next = models.JSONField("label_next", default=list)
    auto_submit = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.project} --- {self.name}"
