from website.models import Project
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods

from datetime import datetime
import requests
import json


class WorkerInterface:
    def __init__(self, project: Project):
        self.project: Project = project

    def schedule_active_learning(self):
        json_data = {
            "settings": self.project.settings,
            "labels": self.project.get_labels(),
            "id": str(self.project.id),
        }

        requests.post("http://localhost:8001/run/", json=json_data)

    def schedule_dataset_prediction(self):
        json_data = {
            "settings": self.project.settings,
            "labels": self.project.get_labels(),
            "id": str(self.project.id),
        }

        requests.post("http://localhost:8001/dataset/", json=json_data)


@csrf_exempt
@require_http_methods(["POST"])
def worker_results_view(request):
    kwargs = json.loads(request.body)
    result_type = kwargs["type"]
    project: Project = Project.objects.get(id=kwargs["id"])

    print("Results -> ", result_type, kwargs["data"])

    match result_type:
        case "active_learning":
            for oracle in project.oracle_set.all():
                oracle.label_next = kwargs["data"]
                oracle.save()
        case "predict_dataset":
            project.predicted_labels = kwargs["data"]["unlabelled"]
            project.test_labels = kwargs["data"]["test"]
            project.save()
        case _:
            print(f"Unknown result type received from worker '{result_type}'")

    return HttpResponse()


@csrf_exempt
@require_http_methods(["POST"])
def worker_stats_view(request):
    kwargs = json.loads(request.body)
    project: Project = Project.objects.get(id=kwargs["id"])
    stats: list = project.stats

    for entry in stats:
        if entry["time"] == kwargs["task_time"]:
            entry.update(kwargs["data"])
            break
    else:
        stats.append({**kwargs["data"], "task": kwargs["type"], "time": kwargs["task_time"]})

    stats.sort(key=lambda stat: datetime.fromisoformat(stat["time"]))

    # for stat_name, stat in kwargs["data"].items():
    #     stat_list = project.stats.setdefault(stat_name, [])
    #     stat_list.append({"task": task_time, "stat": stat, "task_type": kwargs["type"]})
    #     stat_list.sort()
    # stat_list = project.stats.setdefault(stat_name, [])

    # for name, value in kwargs["data"].items():
    #     if isinstance(value, list):
    #         existing_value = project.stats.get(name, [])
    #
    #         if not isinstance(existing_value, list):
    #             existing_value = [value]
    #
    #         project.stats[value] = [*existing_value, *value]
    #     else:
    #         project.stats[name] = value

    project.save()

    return HttpResponse()


@csrf_exempt
@require_http_methods(["POST"])
def worker_error_view(request):
    kwargs = json.loads(request.body)
    print("Error -> ", kwargs["data"])

    return HttpResponse()
