from .base_component_types import InputComponent, BaseComponent
from abc import ABC, abstractmethod
from website.models import Project
from typing import Union


class SelectionComponent(InputComponent, ABC):
    auto_refresh = True
    template = "components/selection.html"
    options: list[str]

    def __init__(self, project: Project):
        super().__init__(project)
        if self.value not in self.options:
            self.value = ""

    @property
    @abstractmethod
    def options(self) -> list[str]:
        ...

    @property
    def enabled(self) -> bool:
        if not super().enabled:
            return False

        return bool(self.options)

    def get_data(self) -> dict:
        return {
            **super().get_data(),
            "options": self.options,
        }

    def submit(self, value: str) -> None:
        if value not in self.options:
            value = ""

        super().submit(value)


class TextComponent(InputComponent, ABC):
    auto_refresh = False
    template = "components/text.html"
    required = False


class TextAreaComponent(InputComponent, ABC):
    auto_refresh = False
    template = "components/textarea.html"
    required = False


class NumberComponent(InputComponent, ABC):
    auto_refresh = False
    template = "components/number.html"
    integer_only = False
    minimum = None
    maximum = None
    required = False

    @property
    @abstractmethod
    def default(self) -> str:
        ...

    def submit(self, value: str) -> None:
        try:
            if self.integer_only:
                num_val = int(value)
            else:
                num_val = float(value)
        except ValueError:
            return

        if self.maximum is not None and num_val > self.maximum:
            return

        if self.minimum is not None and num_val < self.minimum:
            return

        super().submit(value)


class ButtonsComponent(BaseComponent, ABC):
    template = "components/buttons.html"
    danger = False
    required = False
    redirect: str
    buttons: list[dict]

    @property
    @abstractmethod
    def buttons(self) -> list[dict]:
        ...

    def get_data(self) -> dict:
        return {
            **super().get_data(),
            "buttons": [
                {
                    "title": button["title"],
                    "danger": button.get("danger", False),
                    "redirect": button.get("redirect", ""),
                    "require_run_button": button.get("require_run_button", False),
                }
                for button in self.buttons
            ],
        }

    def submit(self, value: str) -> Union[list, None]:
        if not value:
            return None

        return ["project", self.project.name, value]
