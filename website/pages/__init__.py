from .special_pages.authentication import login_view, logout_view
from .special_pages.create import create_view
from .special_pages.delete import delete_view
from .special_pages.home import home_view
from .special_pages.dataset import (generate_dataset_view, dataset_ping_view,
                                    download_test_dataset_view, download_train_dataset_view)

from .project_page import project_view
from .labelling_page import labelling_view, labelling_ping_view
