from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def delete_view(request, project_name):
    context = {
        "project": project_name,
        "header": {
            "project_list": request.user.project_names,
            "show_run_button": False,
        },
    }

    return render(request, "pages/special_pages/delete.html", {"context": context})
