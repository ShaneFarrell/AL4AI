from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt

from website.settings import SOURCE_CODE_ADDRESS
from website.models import User


@csrf_exempt
def login_view(request):
    if request.method == "POST":
        email, password = request.POST["login-email"], request.POST["login-password"]

        user = authenticate(request, email=email, password=password)

        if user is not None:
            login(request, user)
            print("Log In success", email, password)
            return redirect("home")

        print("Log In failure", email, password)
        return redirect(request.build_absolute_uri())

    return render(request, "pages/special_pages/login.html", context={"context": {
        "source_code": SOURCE_CODE_ADDRESS,
    }})


@csrf_exempt
def signup_view(request):
    if request.method == "POST":
        email, password = request.POST["signup-email"], request.POST["signup-password"]

        if User.objects.filter(email=email).first() is None:
            user = User.objects.create_user(email, password)
            login(request, user)
            print("Sign up success", email, password)
            return redirect("home")

        print("Signup failure, user exists", email, password)
        return redirect(request.build_absolute_uri())

    return render(request, "pages/special_pages/signup.html")


@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect("login")
