from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def create_view(request):
    context = {
        "header": {
            "project_list": request.user.project_names,
            "show_run_button": False,
        },
    }

    return render(request, "pages/special_pages/create.html", {"context": context})

