from django.contrib.auth.decorators import login_required
from website.models import Project, User
from django.shortcuts import HttpResponse, redirect
import csv
from django.shortcuts import render
from website.worker import WorkerInterface
from django.utils.text import slugify


@login_required
def download_train_dataset_view(request, project_name):
    user: User = request.user
    project: Project = user.get_project(project_name)

    if project is None:
        return redirect("home")

    if request.method == "GET":
        return download_train_dataset(project)


@login_required
def download_test_dataset_view(request, project_name):
    user: User = request.user
    project: Project = user.get_project(project_name)

    if project is None:
        return redirect("home")

    if request.method == "GET":
        return download_test_dataset(project)


@login_required
def generate_dataset_view(request, project_name):
    user: User = request.user
    project: Project = user.get_project(project_name)

    if project is None:
        return redirect("home")

    project.predicted_labels = None
    project.test_labels = None
    project.download_count += 1
    project.save()
    WorkerInterface(project).schedule_dataset_prediction()

    context = {
        "project": project_name,
        "header": {
            "project_list": request.user.project_names,
            "show_run_button": False,
        },
    }

    return render(request, "pages/special_pages/download.html", {"context": context})


def dataset_ping_view(request, project_name: str):
    user: User = request.user
    project: Project = user.get_project(project_name)

    if project is None:
        return HttpResponse("Cancel")

    if project.predicted_labels is None:
        return HttpResponse("Wait")

    return HttpResponse("Download")


def download_train_dataset(project: Project):
    response = HttpResponse(content_type='text/csv')

    filename = get_download_filename(project, "train")
    response['Content-Disposition'] = f'attachment; filename="{filename}"'

    writer = csv.writer(response, csv.excel)
    response.write('\ufeff'.encode('utf8'))
    writer.writerow(["URL", "Label", "Oracle", "Timestamp"])
    seen_urls = set()

    for label in project.label_set.all():
        seen_urls.add(label.url)
        writer.writerow([label.url, label.value, label.oracle_name, label.created])

    for url, label_value in project.predicted_labels.items():
        if url not in seen_urls:
            writer.writerow([url, label_value, "<generated>", ""])

    return response


def download_test_dataset(project: Project):
    test_labels = project.test_labels
    if not test_labels or not isinstance(test_labels, dict):
        return HttpResponse()

    response = HttpResponse(content_type='text/csv')

    filename = get_download_filename(project, "test")
    response['Content-Disposition'] = f'attachment; filename="{filename}"'

    writer = csv.writer(response, csv.excel)
    response.write('\ufeff'.encode('utf8'))
    writer.writerow(["URL", "Label"])

    for url, label_value in test_labels.items():
        writer.writerow([url, label_value])

    return response


def get_download_filename(project: Project, postfix: str):
    return f"{slugify(project.name).replace('-', '_')}_{postfix}_{project.download_count}.csv"
