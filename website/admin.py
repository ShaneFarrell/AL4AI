from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from .models import User, Project, Oracle, Label


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {"fields": ("email", "password")}),
        # ("Projects", {"fields": ("api_key",)}),
        ("Permissions", {"fields": ("is_active", "is_staff", "is_superuser", "groups", "user_permissions")}),
        ("Important dates", {"fields": ("last_login", "date_joined")}),
    )

    add_fieldsets = (
        (None, {
            "classes": ("wide",),
            "fields": ("email", "password1", "password2"),
        }),
    )

    list_display = ("email", "is_staff")
    search_fields = ("email",)
    ordering = ("email",)


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ("name", "user", "state")
    ordering = ("user", "run_until", "name")


@admin.register(Oracle)
class OracleAdmin(admin.ModelAdmin):
    list_display = ("name", "project")
    ordering = ("project", "name")


@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    list_display = ("project", "url", "value")
    ordering = ("project", "url")
