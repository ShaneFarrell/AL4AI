from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login, logout
from django.utils.timezone import localtime
from datetime import timedelta

from website.interface.component_types import all_components
from website.worker import WorkerInterface
from website.models import Oracle, User, Project
import requests


@require_http_methods(["POST"])
def submit_view(request):
    project_name = request.POST.get("project-name")

    match request.POST.get("action", "update"):
        case "login":
            return action_login(request)
        case "logout":
            return action_logout(request)
        case "start":
            return action_start(request, project_name)
        case "stop":
            return action_stop(request, project_name)
        case "create":
            return action_create(request, project_name)
        case "delete":
            return action_delete(request, project_name)
        case "update":
            return action_update(request, project_name)
        case "label":
            return action_label(request)

    return redirect("home")


def action_login(request) -> redirect:
    email, password = request.POST["login-email"], request.POST["login-password"]

    user = authenticate(request, email=email, password=password)

    if user is not None:
        login(request, user)
        return redirect("home")

    return redirect("login")


def action_label(request) -> redirect:
    oracle_id = request.POST.get("oracle", "")
    oracle: Oracle = Oracle.objects.filter(id=oracle_id).first()

    result = request.POST.get("result")
    datum_url = request.POST.get("datum")
    auto_submit = bool(request.POST.get("auto-submit"))

    if result and oracle and datum_url in oracle.label_next:
        new_label_next = [label for label in oracle.label_next if label != datum_url]
        oracle.project.add_label(datum_url, result, oracle.name)
        oracle.label_next = new_label_next
        oracle.auto_submit = auto_submit

        oracle.save()

        if not new_label_next and oracle.project.running:
            WorkerInterface(oracle.project).schedule_active_learning()

    return redirect("labelling", oracle_id)


@login_required
def action_logout(request) -> redirect:
    logout(request)
    return redirect("login")


@login_required
def action_delete(request, project_name: str) -> redirect:
    user: User = request.user
    project = user.get_project(project_name)

    if project is not None:
        project.delete()

    return redirect("home")


@login_required
def action_create(request, project_name: str) -> redirect:
    user: User = request.user

    try:
        project = user.create_project(project_name)
    except (KeyError, ValueError):
        return redirect("create")

    return action_update(request, project)


@login_required
def action_update(request, project_name: str) -> redirect:
    user: User = request.user
    project = user.get_project(project_name)

    if project is not None:
        component_redirect = None

        for component in all_components:
            component = component(project)
            value = request.POST.get(f"project-{component.name}", component.value)
            component_redirect = component.submit(value) or component_redirect

        project.save()

        if component_redirect is not None:
            return redirect(*component_redirect)

    if "redirect" in request.POST:
        return redirect(request.POST["redirect"])

    if project:
        return redirect("project", project.name, "_")

    return redirect("home")


@login_required
def action_start(request, project_name: str) -> redirect:
    user: User = request.user
    project: Project = user.get_project(project_name)

    if project is None:
        return redirect("home")

    for component in all_components:
        component = component(project)
        component.submit(request.POST.get(f"project-{component.name}", component.value))

    oracles: list[str] = project.settings.get("Oracles", "").split(",")
    oracles = [oracle.strip() for oracle in oracles if oracle.strip()]
    oracles = oracles[:1]  # Max of 1 oracle for now
    project.set_oracles(oracles)

    project.save()

    try:
        WorkerInterface(project).schedule_active_learning()
    except requests.exceptions.ConnectionError:
        pass
    else:
        project.run_until = localtime() + timedelta(days=30)
        project.save()

    return redirect("project", project_name, "_")


@login_required
def action_stop(request, project_name: str) -> redirect:
    user: User = request.user
    project = user.get_project(project_name)

    if project is None:
        return redirect("home")

    for oracle in project.oracle_set.all():
        oracle.label_next = []
        oracle.save()

    project.run_until = localtime()
    project.save()

    return redirect("project", project_name, "_")
